<?php

namespace Classes;

use Classes\Containers\Container;
use Classes\Interfaces\FigureInterface;

class Logistic
{
    private array $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    public function getContainers(): array
    {
        return $this->params['containers'];
    }

    public function getTransports(): array
    {
        return $this->params['transports'];
    }

    public function run(): void
    {
        $arrayContainers = $this->getContainers();

        /**
         * @var Container[] $containers
         */
        $containers = [];

        foreach ($arrayContainers as $container) {
            $containers[$container['type']] = new Container($container);
        }

        usort($containers, function (Container $a, Container $b) {
           return $a->square - $b->square;
        });

        $arrayTransports = $this->getTransports();
        $transports = [];

        foreach ($arrayTransports as $key => $transport) {
            foreach ($transport as $figure) {
                $newFigure = $figure['class'];
                $transports[$key][] = new $newFigure($figure);
            }
            usort($transports[$key], function (FigureInterface $a, FigureInterface $b) {
                return $a->calculateSquare() - $b->calculateSquare();
            });
        }

        $usedContainers = [];
        $errors = [];

        foreach ($transports as $transportIndex => $transport) {
            foreach ($containers as $containerIndex => $containerScheme) {
                $container = clone $containerScheme;
                $usedContainers[$transportIndex][] = $container;
                foreach ($transport as $figure) {
                    if (!$container->canBePut($figure)) {
                        if ($containerIndex == count($containers) - 1) {

                            $errors[] = 'Figure cannot be placed. ' . serialize($figure);
                            continue;
                        }
                        continue 2;
                    }

                    if ($container->fits($figure)) {
                        $container->put($figure);
                    } else {
                        $container = clone $containerScheme;
                        $usedContainers[$transportIndex][] = $container;
                        $container->put($figure);
                    }

                }

                if (!$container->isUsed()) {
                    unset($usedContainers[$transportIndex][count($usedContainers[$transportIndex]) - 1]);
                }
            }
        }

        $this->console($usedContainers, $errors);
    }

    public function console($result, $errors): void
    {
        foreach ($result as $key => $item) {
            $types = array_map(function ($value) {
                return $value->type;
            }, $item);
            $transport = array_count_values($types);
            echo 'Transport # ' . $key + 1 . ' need ' . json_encode($transport) . PHP_EOL;
        }
        if ($errors) {
            echo 'Also we have errors: ' . json_encode($errors) . PHP_EOL;
        }
    }
}