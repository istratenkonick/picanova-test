<?php

namespace Classes\Objects;

use Classes\Interfaces\FigureInterface;

class Circle implements FigureInterface
{
    public float $radius;
    public bool  $packed = false;

    public function __construct($params)
    {
        $this->radius = $params['radius'];
    }

    public function calculateSquare(): float
    {
        $width = $length = $this->radius * 2;

        return $width * $length;
    }

    public function pack()
    {
        $this->packed = true;
    }

    public function isPacked(): bool
    {
        return $this->packed;
    }
}
