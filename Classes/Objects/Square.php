<?php

namespace Classes\Objects;

use Classes\Interfaces\FigureInterface;

class Square implements FigureInterface
{
    public float $width;
    public float $length;
    public bool  $packed = false;

    public function __construct($params)
    {
        $this->width = $params['width'];
        $this->length = $params['length'];
    }

    public function calculateSquare(): float
    {
       return $this->width * $this->length;
    }

    public function pack()
    {
        $this->packed = true;
    }

    public function isPacked(): bool
    {
        return $this->packed;
    }
}
