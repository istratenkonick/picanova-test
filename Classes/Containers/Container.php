<?php

namespace Classes\Containers;

use Classes\Interfaces\FigureInterface;

class Container
{
    public float  $width;
    public float  $length;
    public float  $square;
    public float  $usedSquare = 0;
    public string $type;
    public array  $figures = [];

    public function __construct($params)
    {
        $this->width = $params['width'];
        $this->length = $params['length'];
        $this->square = $this->width * $this->length;
        $this->type = $params['type'];
    }

    /**
     * @param FigureInterface $figure
     * @return bool
     */
    public function canBePut(FigureInterface $figure): bool
    {
        return $figure->calculateSquare() <= $this->square;
    }

    /**
     * @param FigureInterface $figure
     * @return bool
     */
    public function fits(FigureInterface $figure): bool
    {
        return $figure->calculateSquare() <= $this->square - $this->usedSquare;
    }

    /**
     * @param FigureInterface $figure
     * @return void
     */
    public function put(FigureInterface $figure): void
    {
        if ($figure->isPacked()) {
            return;
        }

        $this->usedSquare += $figure->calculateSquare();
        $this->figures[] = $figure;
        $figure->pack();
    }

    public function isUsed(): bool
    {
        return $this->usedSquare > 0;
    }
}
