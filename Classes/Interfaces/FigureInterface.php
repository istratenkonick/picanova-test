<?php

namespace Classes\Interfaces;

interface FigureInterface
{
    public function calculateSquare();

    public function pack();

    public function isPacked(): bool;
}
