<?php

use Classes\Logistic;

require_once './bootstrap.php';

$params = json_decode(file_get_contents('task.json'), true);

$logistic = new Logistic($params);

$logistic->run();

