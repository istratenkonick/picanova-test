<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (file_exists('./vendor/autoload.php')) {
    require_once './vendor/autoload.php';
}

spl_autoload_register(function ($className) {
    $className = str_replace('\\', '/', $className);
    include __DIR__ . "/$className.php";
});